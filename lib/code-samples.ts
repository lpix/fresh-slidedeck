import Prism from "prism";
import "prism-jsx";

type Lang = "javascript" | "jsx" | "css" | "html" | "python" | "php";

export const highlight =
  (lang: Lang | undefined) => (code: string | undefined) => {
    if (!code) return "";
    if (!lang) return code;

    try {
      return Prism.highlight(code, Prism.languages[lang], lang);
    } catch (e) {
      console.warn("Prism highlighting failed", e);
      return code;
    }
  };

export const slide4Code = highlight("jsx")(`
const App = ({ title, items, handleLoadMore }) => {
  return (
    <div>
      {title ? <h2>{title}</h2> : null}

      <ul>
        {items.map((item) => <li key={item.key}>{item.label}</li>)}
      </ul>

      {handleLoadMore
        ? <button type="button" onClick={handleLoadMore}>Load More</button>
        : null}
    </div>
  );
};
`);

export const slide6Code = highlight("jsx")(`
const App = ({ id }) => {
  const [items, setItems] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState(null);
  
  useEffect(() => {
    // ...
  }, [id]);
}
`);

export const slide8Code = highlight("jsx")(`
// routes/[slide].tsx
<h2>Welcome to Fresh!</h2>
<Counter initialCount={3} />

// islands/Counter.tsx
export default function Counter(props: CounterProps) {
  const [count, setCount] = useState(props.initialCount);

  return (
    <div>
      <Button onClick={() => setCount((value) => value - 1)}>-1</Button>
      <p>{count}</p>
      <Button onClick={() => setCount((value) => value + 1)}>+1</Button>
    </div>
  );
}
`);

export const slide9Code = highlight("javascript")(`
import { assertEquals } from "https://deno.land/std@0.204.0/assert/assert_equals.ts";

Deno.test("Deno test runner demo", () => {
  const result = [1, 2, 3].indexOf(4);
  assertEquals(result, -1);
});
`);

export const slide10Code = highlight("jsx")(`
export default function Counter(props) {
  const [count, setCount] = useState(props.initialCount);

  return (
    <div>
      <Button onClick={() => setCount((value) => value - 1)}>-1</Button>
      <p>{count}</p>
      <Button onClick={() => setCount((value) => value + 1)}>+1</Button>
    </div>
  );
}
`);
