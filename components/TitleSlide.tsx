import { Fragment } from "preact/jsx-runtime";
import Hero from "./Hero.tsx";
import { Head } from "$fresh/runtime.ts";

const TitleSlide = () => {
  return (
    <Fragment>
      <Head>
        <link rel="stylesheet" type="text/css" href="/styles.css" />
      </Head>
      <section className="slide slide--title flex flex-col" id="slide">
        <Hero title="A Fresh Look at Isomorphic Rendering" />

        <div className="flex flex-col justify-around items-center h-full">
          <h2 className="text-h2">Casey A. Ydenberg</h2>
          <p>https://gitlab.com/lpix/fresh-slidedeck</p>
        </div>
      </section>
    </Fragment>
  );
};

export default TitleSlide;
