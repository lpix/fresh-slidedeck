import { Fragment, FunctionComponent } from "preact";
import { Head } from "$fresh/runtime.ts";

interface Props {
  title?: string;
  margin?: boolean;
}

const Slide: FunctionComponent<Props> = ({ children, title }) => {
  return (
    <Fragment>
      <Head>
        <link rel="stylesheet" type="text/css" href="/styles.css" />
      </Head>
      <section className="slide">
        {title
          ? (
            <header className="bg-blue">
              <h1 className="inline-block text-h1/8 text-white my-10 pr-6 pb-2 pl-20 border-b-[4px] border-green lps-br-dot">
                {title}
              </h1>
            </header>
          )
          : null}
        {children}
      </section>
    </Fragment>
  );
};

export default Slide;
