export const GREEN = "hsl(73, 96%, 38%)";
export const GREEN_HALF = "hsla(73, 96%, 38%, 35%)";
export const RED = "hsl(0, 100%, 50%)";
export const RED_HALF = "hsla(0, 100%, 50%, 35%)";
export const BLUE = "hsl(190, 60%, 19%)";
export const GRAY = "hsl(180, 5%, 89%)";
