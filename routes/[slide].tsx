import Slide from "../components/Slide.tsx";
import { PageProps } from "$fresh/server.ts";
import {
  slide10Code,
  slide4Code,
  slide6Code,
  slide8Code,
  slide9Code,
} from "../lib/code-samples.ts";
import Counter from "../islands/Counter.tsx";
import Keytrap from "../islands/Keytrap.tsx";
import { Fragment } from "preact/jsx-runtime";
import TitleSlide from "../components/TitleSlide.tsx";

export const SLIDES = [
  <TitleSlide />,

  <Slide title="The History of Medicine">
    <div className="text-h2 m-10 flex flex-row justify-center">
      <div>
        <p>2000 B.C. - "Here, eat this root."</p>
        <p>1000 B.C. - "That root is heathen, say this prayer."</p>
        <p>1850 A.D. - "That prayer is superstition, drink this potion."</p>
        <p>1940 A.D. - "That potion is snake oil, swallow this pill."</p>
        <p>1985 A.D. - "That pill is ineffective, take this antibiotic."</p>
        <p>
          2000 A.D. - "That antibiotic is artificial. Here, eat this root!"
        </p>
      </div>
    </div>
  </Slide>,

  <Slide title="The History of Web Development">
    <div className="text-h2 m-10 flex flex-row justify-center">
      <div>
        <p>1990s - Serve static HTML</p>
        <p>2000s - Retrieve data from a database, render HTML on a server</p>
        <p>
          2010s - Retrieve JavaScript and a data from a server, render
          everything inside the browser
        </p>
        <p>2020s - Serve static HTML</p>
      </div>
    </div>
  </Slide>,

  <Slide title="Alex Russell: The Market for Lemons">
    <div className="text-h2 m-10 flex flex-row justify-center">
      <div className="text-base mr-8">
        <p>
          <a
            href="https://infrequently.org/2023/02/the-market-for-lemons"
            className="text-green"
          >
            https://infrequently.org/2023/02/the-market-for-lemons
          </a>
        </p>
        <p>
          <a
            href="https://seldo.com/posts/the_case_for_frameworks"
            className="text-green"
          >
            https://seldo.com/posts/the_case_for_frameworks
          </a>
        </p>
      </div>
      <div>
        <p>Summary:</p>
        <ul className="list-disc list-inside">
          <li>
            SPAs open up lots of complexity that doesn't scale down well if you
            aren't Facebook
          </li>
          <li>
            The poor performance of <em>delivering</em>{" "}
            React (or related) outweighs the user experience gains
          </li>
          <li>
            Rendering on the client puts more computational load on users (esp.
            CSS-in-JS) (esp. mobile/Android)
          </li>
        </ul>
      </div>
    </div>
  </Slide>,

  <Slide title={`Is React Really "Too Complex?"`}>
    <pre
      data-lang="jsx"
      className="language-jsx text-base"
    ><code dangerouslySetInnerHTML={{
      __html: slide4Code
    }}></code></pre>
  </Slide>,

  // <Slide title="The Ideal: Isomorphic Rendering">
  //   <div className="text-h2 m-10 flex flex-row justify-center">
  //     <ul className="list-disc list-inside">
  //       <li>(Handlebars + jQuery)</li>
  //       <li>Express/React-dom renderToString</li>
  //       <li>Client-side hydration</li>
  //       <li>Apollo</li>
  //       <li>JAM stack</li>
  //       <li>Suspense</li>
  //       <li>React server-components</li>
  //     </ul>
  //   </div>
  // </Slide>,

  <Slide title="Write SPA, Then Add SSR Pixie-dust">
    <div className="text-h2 m-10 flex flex-row justify-center">
      <pre
        data-lang="jsx"
        className="language-jsx text-base"
      ><code dangerouslySetInnerHTML={{
        __html: slide6Code
      }}></code></pre>
      <ul className="list-disc list-inside ml-10">
        <li>"Isomorphism" usually works for view layer only</li>
        <li>JS bundles get too big</li>
        <li>App state has to exactly match</li>
        <li>Routing and history introduce additional complexity</li>
      </ul>
    </div>
  </Slide>,

  <Slide title="My Wishlist Stack">
    <div className="text-h2 m-10 flex flex-row justify-center">
      <ul className="list-disc list-inside ml-10">
        <li>
          Full-power of an MVC serverside framework (inc. dynamically rendered
          pages on the <em>server</em>)
        </li>
        <li>Use JSX as a templating language</li>
        <li>
          Deliver <em>just enough</em> JS to get the interactivity needed
        </li>
        <li>
          That JS can still fetch data from an API <em>on the same server</em>
          {" "}
          if it needs to
        </li>
      </ul>
    </div>
  </Slide>,

  <Slide title="Enter ... Fresh">
    <div className="flex flex-row justify-center">
      <pre
        data-lang="jsx"
        className="language-jsx text-base"
      ><code dangerouslySetInnerHTML={{
        __html: slide8Code
      }}></code></pre>
      <div className="ml-10">
        <h2>Enter ... Fresh</h2>
        <Counter initialCount={3} />
      </div>
    </div>
  </Slide>,

  <Slide title="What is Deno?">
    <div className="flex flex-col justify-center">
      <pre
        data-lang="jsx"
        className="language-jsx text-base"
      ><code dangerouslySetInnerHTML={{
        __html: slide9Code
      }}></code></pre>
      <div className="m-10 text-h2">
        <h2 className="text-green">Deno Features:</h2>
        <ul>
          <li>TypeScript and JSX Just Work</li>
          <li>Formatting, Typechecking, Linting, Test runners Just Work</li>
          <li>ES Modules by default</li>
        </ul>
      </div>
    </div>
  </Slide>,

  <Slide title="What is Preact?">
    <div className="flex flex-row justify-center">
      <pre
        data-lang="jsx"
        className="language-jsx text-base"
      ><code dangerouslySetInnerHTML={{
        __html: slide10Code
      }}></code></pre>
      <div className="m-10 text-h2">
        <h2 className="text-green">Preact Features:</h2>
        <ul>
          <li>Like React, but 3kb</li>
        </ul>
      </div>
    </div>
  </Slide>,

  <Slide title="What Does a Fresh Project Look Like?">
    <p>(show me the code!)</p>
  </Slide>,

  null,

  <Slide title="What can you build with Fresh?">
    <div className="flex flex-row justify-center">
      <ul className="m-10 text-h2">
        <li>
          <a href="https://fresh.deno.dev/" className="text-green">
            Fresh website
          </a>
        </li>
        <li>
          <a href="https://liberchat.deno.dev/" className="text-green">
            LiberChat
          </a>
        </li>
        <li>
          <a
            href="https://livingpixel.io/blog/world-demographics-in-2100"
            className="text-green"
          >
            Living Pixel Solutions
          </a>
        </li>
      </ul>
    </div>
  </Slide>,

  <Slide title="What's missing?">
    <div className="flex flex-row justify-center">
      <ul className="m-10 text-h2">
        <li>CSS (Twind)</li>
        <li className="invisible">The M in MVC</li>
        <li className="invisible">
          Deno is not Node! (CommonJS does not work and isn't going to).
        </li>
      </ul>
    </div>
  </Slide>,

  <Slide title="What's missing?">
    <div className="flex flex-row justify-center">
      <ul className="m-10 text-h2">
        <li>CSS (Twind)</li>
        <li>The M in MVC</li>
        <li className="invisible">
          Deno is not Node! (CommonJS does not work and isn't going to).
        </li>
      </ul>
    </div>
  </Slide>,

  <Slide title="What's missing?">
    <div className="flex flex-row justify-center">
      <ul className="m-10 text-h2">
        <li>CSS (Twind)</li>
        <li>The M in MVC</li>
        <li>Deno is not Node! (CommonJS does not work and isn't going to).</li>
      </ul>
    </div>
  </Slide>,

  <Slide title="Learn more">
    <div className="text-h2 m-10 flex flex-row justify-center">
      <ul className="text-base mr-8">
        <li>
          <a
            href="https://infrequently.org/2023/02/the-market-for-lemons"
            className="text-green"
          >
            https://infrequently.org/2023/02/the-market-for-lemons
          </a>
        </li>
        <li>
          <a
            href="https://seldo.com/posts/the_case_for_frameworks"
            className="text-green"
          >
            https://seldo.com/posts/the_case_for_frameworks
          </a>
        </li>
        <li>
          <a
            href="https://fresh.deno.dev/"
            className="text-green"
          >
            https://fresh.deno.dev/
          </a>
        </li>
        <li>
          <a
            href="https://fresh.deno.dev/"
            className="text-green"
          >
            https://deno.com/blog/web-frameworks-on-deno
          </a>
        </li>
        <li>
          <a
            href="https://gitlab.com/lpix/fresh-slidedeck"
            className="text-green"
          >
            https://gitlab.com/lpix/fresh-slidedeck
          </a>
        </li>
      </ul>
      <div>
        <p className="text-green">https://livingpixel.io</p>
      </div>
    </div>
  </Slide>,
];

export default function SlideDeck({ params }: PageProps) {
  const slide = parseInt(params.slide);

  return (
    <Fragment>
      <Keytrap totalSlides={SLIDES.length} currentSlide={slide} />
      {SLIDES[slide]}
    </Fragment>
  );
}
