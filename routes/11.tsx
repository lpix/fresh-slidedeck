import { Handlers, PageProps } from "$fresh/server.ts";

import Slide from "../components/Slide.tsx";
import { getJoke } from "../lib/getJoke.ts";
import DisplayJoke from "../islands/DisplayJoke.tsx";

import { SLIDES } from "./[slide].tsx";
import Keytrap from "../islands/Keytrap.tsx";

interface Props {
  joke: string;
}

export const handler: Handlers = {
  async GET(_req, context) {
    const joke = await getJoke();
    return context.render({
      joke,
    });
  },

  async POST(request, context) {
    const body = await request.formData();
    return context.render({
      joke: body.get("your-joke"),
    });
  },
};

export default function Slide11({ data }: PageProps<Props>) {
  return (
    <Slide title="A More Involved Isomorphic Example">
      <Keytrap totalSlides={SLIDES.length} currentSlide={11} />

      <div className="flex flex-col justify-center">
        <DisplayJoke joke={data.joke} />
      </div>

      <form
        method="POST"
        className="m-10 text-center text-h3 flex flex-col items-center"
      >
        <label htmlFor="your-joke" className="block">Submit your own</label>
        <textarea
          id="your-joke"
          name="your-joke"
          className="block text-gray-dark text-base px-4"
        />
        <button type="submit" className="bg-blue inline my-4 px-4 py-1 rounded">
          Send!
        </button>
      </form>
    </Slide>
  );
}
