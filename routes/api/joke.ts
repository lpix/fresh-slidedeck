import { Handlers } from "$fresh/server.ts";
import { getJoke } from "../../lib/getJoke.ts";

export const handler: Handlers = {
  async GET() {
    const joke = await getJoke();
    return new Response(JSON.stringify({ joke }), {
      headers: {
        "Content-Type": "application/json",
      },
    });
  },
};
