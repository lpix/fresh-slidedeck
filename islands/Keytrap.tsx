import { useEffect, useState } from "preact/hooks";

interface Props {
  currentSlide: number;
  totalSlides: number;
}

const navigate = (url: string) => {
  // deno-lint-ignore no-explicit-any
  globalThis.location = url as any;
};

export const useFullscreen = () => {
  const [isFullscreen, setFullscreen] = useState(false);

  useEffect(() => {
    const el = document.body;
    const onFullscreenChange = () => {
      setFullscreen(!!document.fullscreenElement);
    };

    if (isFullscreen) {
      el.requestFullscreen();
      el.addEventListener(
        "fullscreenchange",
        onFullscreenChange,
      );
    } else if (!isFullscreen && document.fullscreenElement) {
      document.exitFullscreen();
    }

    return () => {
      el.removeEventListener(
        "fullscreenchange",
        onFullscreenChange,
      );
    };
  }, [isFullscreen]);

  return { isFullscreen, setFullscreen };
};

export default ({ currentSlide, totalSlides }: Props) => {
  const prev = currentSlide >= 1 ? currentSlide - 1 : 0;
  const next = currentSlide < totalSlides ? currentSlide + 1 : totalSlides - 1;

  const { setFullscreen } = useFullscreen();

  useEffect(() => {
    globalThis.addEventListener("keyup", (ev) => {
      switch (ev.key) {
        case "ArrowRight":
        case "Enter":
        case " ":
          navigate(`/${next}`);
          return;

        case "ArrowLeft":
          navigate(`/${prev}`);
          return;

        case "f":
          setFullscreen(true);
          return;

        case "Esc":
          setFullscreen(false);
          return;
      }
    });
  }, []);

  return null;
};
