import { useCallback, useState } from "preact/hooks";

interface Props {
  joke: string;
}

export default function DisplayJoke({ joke }: Props) {
  const { value, handleRefresh } = useJokeState(joke);

  return (
    <div className="m-10 text-center">
      <h2 className="text-h2">{value}</h2>
      <button
        type="button"
        className="bg-green px-4 py-1 rounded"
        onClick={handleRefresh}
      >
        Refresh
      </button>
    </div>
  );
}

const useJokeState = (initial: string) => {
  const [value, setValue] = useState(initial);

  const handleRefresh = useCallback(async () => {
    const res = await fetch(`/api/joke`, {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    });

    const body = await res.json();
    setValue(body.joke as string);
  }, []);

  return {
    value,
    handleRefresh,
  };
};
