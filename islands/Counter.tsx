import { Button } from "../components/Button.tsx";
import { useState } from "preact/hooks";

interface CounterProps {
  initialCount: number;
}

export default function Counter(props: CounterProps) {
  const [count, setCount] = useState(props.initialCount);

  return (
    <div class="flex gap-2 py-6">
      <Button
        onClick={() => setCount((value) => value - 1)}
        className="bg-green p-2 rounded"
      >
        -1
      </Button>
      <p class="text-3xl">{count}</p>
      <Button
        onClick={() => setCount((value) => value + 1)}
        className="bg-green p-2 rounded"
      >
        +1
      </Button>
    </div>
  );
}
