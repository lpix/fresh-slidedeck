# Fresh Slide Deck

This is a slide deck built on the [Fresh](https://fresh.deno.dev) framework. It handles slide selection, navigation with the arrow keys, and full screen mode. It also implements a serverside code syntax highlighter with Prism.js.

You may use it as a starting point for another presentation, under the [MIT license](https://opensource.org/license/mit/).

### Usage

Make sure to install Deno: https://deno.land/manual/getting_started/installation

Then start the project:

```
deno task start
```

This will watch the project directory and restart as necessary.
